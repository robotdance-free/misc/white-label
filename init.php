<?php
namespace rd\bd\misc\whitelabel;

function rd_bd_white_label( string $title = '', string $text_color = '#ffffff', string $background_color = '#000000', string $dash_icon = '' ) 
{

	$label = 'Breakdance';
	add_filter(
		'gettext',
		function( $translated_text, $text, $domain ) use ( $title, $label ) {
			    $label = 'Breakdance';
			    if( is_admin() ) {
				    if ( strpos( $translated_text, $label ) !== false ) {
					    $translated_text = str_replace( $label, $title, $translated_text );
				    }
			    }
			    return $translated_text;
		    }, 20, 3
	);
  	add_action(
	    'admin_menu',
	    function() use ( $title, $icon, $label ) {
		    global $menu;
		    $i = 0;
		    foreach( $menu as $key => $item  ) {
			    if( is_array( $item ) && isset( $item[0] ) && $item[0] == $label ) {
				    $menu[$key][0] = $title;
				    $menu[$key][6] = $icon != '' ? $icon : 'dashicons-admin-customizer';
				    break;
			    }
			    $i++;
		    }
	    }
    );
    add_action(
	    'admin_bar_menu',
	    function( $wp_admin_bar ) use( $title ) {
		    $bd_node = $wp_admin_bar->get_node( 'breakdance_admin_bar_menu' );
		    if( $bd_node ) {
			    $bd_node->title = $title;
			    $wp_admin_bar->remove_node('breakdance_admin_bar_menu');
			    $wp_admin_bar->add_node($bd_node);
		    }
	    }, 99999999999, 1
    );
    add_filter(
	    'display_post_states',
	    function( $post_states, $post ) use ( $title ) {
		    if( isset( $post_states[ 'breakdance' ] ) ) {
			    $post_states[ 'breakdance' ] = $title;
		    }
		    return $post_states;
	    } ,10, 2
    );
    add_action(
	    'current_screen',
	    function() use ( $title ) {
		    $current_screen = get_current_screen();
		    if ($current_screen && isset($current_screen->post_type)) {
			    add_filter(
				    "{$current_screen->post_type}_row_actions",
				    function( $actions, $post ) use ( $title ) {
					    if (isset($actions['breakdance_quick_action_link'])) {
						    $actions['breakdance_quick_action_link'] = preg_replace(
							    '/(<a href=[^>]*breakdance=builder[^>]*>)([^<]+)(<\/a>)/i',
							    '$1'.$title.'$3',
							    $actions['breakdance_quick_action_link']
						    );
					    }
					    return $actions;
				    }, 99999, 2
			    );
		    }
	    }
    );
    add_action(
	    'admin_head',
	    function() use( $title, $text_color, $background_color ) {
		    ob_start();
		    ?>
		    <style type="text/css">
			    html body .breakdance-launcher__description {
				    display: none;
			    }
			    html body .breakdance-launcher__buttons {
				    display: flex;
				    flex-direction: row;
				    align-items: center;
			    }
                html body .breakdance-launcher-button,
                html body .breakdance-launcher-small-button {
                    font-size: 0;
                    color: transparent;
                    position: relative;
                    background-color: <?php echo $background_color ?>;
                }
                html body .breakdance-launcher-button:hover,
                html body .breakdance-launcher-small-button:hover {
                    background-color: <?php echo $background_color ?>;
                }
                html body .breakdance-launcher-small-button::after,
                html body .breakdance-launcher-button::after {
                    position: relative;
                    font-size: 16px;
                    content: "<?php echo $title ?>";
                    text-indent: 0;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    white-space: nowrap;
                    color: <?php echo $text_color ?>;
                }
                html body .breakdance-launcher-button:hover::after,
                html body .breakdance-launcher-small-button:hover::after {
                    color: <?php echo $text_color ?>;
                }
                html body .breakdance-launcher-button::after {
                    font-size: 16px;
                }
                html body .breakdance-launcher-small-button::after {
                    font-size: 13px;
                }
		    </style>
		    <?php
		    echo ob_get_clean();
	    }
    );

}

rd_bd_white_label( 'My Builder', '#ffffff', '#000000', 'dashicons-admin-appearance' );
